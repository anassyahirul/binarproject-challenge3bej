package com.app.grouping;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class GroupBySameValue implements Grouping{

    private final HashMap<Integer, Integer> groupedData = new HashMap<>();

    public HashMap<Integer, Integer> getGroupedData() {
        return this.groupedData;
    }

    @Override
    public void putData(List<Integer> data) {
        Integer prevElement = null;
        int count;

        for (Integer number : data) {
            if (!Objects.equals(prevElement, number)) {
                count = 0;
                for (Integer number2 : data) {
                    if (Objects.equals(number2, number)) {
                        count++;
                    }
                }
                this.groupedData.put(number, count);
                prevElement = number;
            }
        }
    }

}
