package com.app.grouping;


import java.util.List;

@FunctionalInterface
public interface Grouping {
    void putData(List<Integer> data);
}
