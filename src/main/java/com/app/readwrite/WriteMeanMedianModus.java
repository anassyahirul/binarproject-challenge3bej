package com.app.readwrite;

import com.app.calculation.CalculationImpl;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class WriteMeanMedianModus extends ReadFile implements WriteData{


    CalculationImpl calculation = new CalculationImpl();

    @Override
    public void write(String fileName) {
        List<Integer> dataNilai = super.read(fileNameRead, ";");
        try {
            File file = new File(resultDirectory + "/" + fileName);
            if (file.createNewFile()) {
                System.out.println("new File is being created");
            }
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Pengolahan Nilai:\n\n");
            bwr.write("Berikut hasil sebaran data nilai\n");

            bwr.write("Mean: " + calculation.getMean(dataNilai));
            bwr.newLine();

            bwr.write("Median: "+ calculation.getMedian(dataNilai));
            bwr.newLine();

            bwr.write("Modus: " + calculation.getModus(dataNilai));
            bwr.flush();
            bwr.close();
            System.out.println("Success writing to a file");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
