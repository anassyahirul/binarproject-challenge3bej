package com.app.readwrite;

@FunctionalInterface
public interface WriteData {

    void write(String fileName);

}
