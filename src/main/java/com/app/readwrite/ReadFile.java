package com.app.readwrite;

import com.app.util.CreateDirectoryUtil;
import com.app.util.IsNumericUtil;
import com.app.view.ViewFileNotFound;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadFile implements ReadData {

//    instance Util
    IsNumericUtil isNumeric = new IsNumericUtil();
    CreateDirectoryUtil createDirectoryUtil = new CreateDirectoryUtil();

    String fileNameRead = "data_sekolah.csv";
    String resultDirectory = "FileOutput";

    @Override
    public List<Integer> read(String fileName, String delimiter) {

        createDirectoryUtil.createDir(resultDirectory);
        try {
            File file = new File(fileName);

            List<Integer> dataList = new ArrayList<>();
            if (file.exists()){
                FileReader fr = new FileReader(file);
                BufferedReader br = new BufferedReader(fr);
                String line;
                String[] tempArr;
                while ((line = br.readLine()) != null) {
                    tempArr = line.split(delimiter);
                    Arrays.stream(tempArr)
                            .filter(isNumeric)
                            .map(Integer::parseInt)
                            .forEach(dataList::add);
                }
                br.close();
            }else {
                ViewFileNotFound fileNotFound = new ViewFileNotFound();
                fileNotFound.show();
            }
            return dataList;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
