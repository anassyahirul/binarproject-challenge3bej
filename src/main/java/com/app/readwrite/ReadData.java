package com.app.readwrite;

import java.util.List;

public interface ReadData {

    List<Integer> read(String fileName, String delimiter);

}
