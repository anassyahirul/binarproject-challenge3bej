package com.app.readwrite;

import com.app.grouping.GroupBySameValue;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

public class WriteDataGrouping extends ReadFile implements WriteData{

    GroupBySameValue group = new GroupBySameValue();

    @Override
    public void write(String fileName) {

        List<Integer> dataNilai = super.read(fileNameRead, ";");

        try {
            File file = new File(resultDirectory + "/" + fileName);
            if (file.createNewFile()) {
                System.out.println("new File is being created");
            }

            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);
            bwr.write("Hasil Pengolahan Nilai:\n\n");
            bwr.newLine();
            bwr.write("-------------------------------------------------------------------");
            bwr.newLine();
            bwr.newLine();

            group.putData(dataNilai);
            HashMap<Integer, Integer> nilaiHashMap = group.getGroupedData();
            nilaiHashMap.forEach((key, value) -> {
                try {
                    bwr.write(key + "        |           " + value);
                    bwr.newLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            bwr.newLine();
            bwr.flush();
            bwr.close();
            System.out.println("Success writing to a file");
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

}
