package com.app.view;

import com.app.util.InputUtil;

public class ViewFileNotFound implements View{

    InputUtil input = new InputUtil();
    ViewMainMenu mainMenu = new ViewMainMenu();

    @Override
    public void show() {
        System.out.println("----------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("----------------------------");

        System.out.println("File Tidak Ditemukan");
        System.out.println("1. Kembali ke menu utama");
        System.out.println("0. Exit");

        switch (input.promptInput()) {
            case 0 -> {
                System.out.println("Program sedang ditutup");
                System.exit(0);
            }
            case 1 -> mainMenu.show();
            default -> {
                System.out.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.show();
            }
        }

    }
}
