package com.app.view;

@FunctionalInterface
public interface View {
    void show();
}
