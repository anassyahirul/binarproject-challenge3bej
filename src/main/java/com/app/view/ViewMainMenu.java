package com.app.view;

import com.app.readwrite.WriteDataGrouping;
import com.app.readwrite.WriteMeanMedianModus;
import com.app.util.InputUtil;

public class ViewMainMenu implements View{

//    ViewFileNotFound fileNotFound = new ViewFileNotFound();
    WriteMeanMedianModus writeMMM = new WriteMeanMedianModus();
    WriteDataGrouping writeGrouping = new WriteDataGrouping();
    InputUtil input = new InputUtil();

    @Override
    public void show() {
        System.out.println("----------------------------");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("----------------------------");

        System.out.println("Letakkan file data_sekolah.csv di folder yang sama dengan aplikasi ini ");
        System.out.println("Output akan di generate ke folder \"FileOutput\" yang baru saja dibuat saat menjalankan aplikasi ini\n");
        System.out.println("Silakan pilih menu");
        System.out.println("1. Generate File untuk menampilkan mean, median, dan modus");
        System.out.println("2. Generate file untuk menampilkan pengelompokkan data");
        System.out.println("3. Generate kedua file");
        System.out.println("0. Exit");

        String fileNameSebaranData = "Result_mean_median_modus.txt";
        String fileNamePengelompokkan = "Result_pengelompokkan.txt";

        ViewDoneMenu done = new ViewDoneMenu();

        switch (input.promptInput()) {
            case 0 -> {
                System.out.println("Program sedang ditutup");
                System.exit(0);
            }
            case 1 -> {
                writeMMM.write(fileNameSebaranData);
                done.show();
            }
            case 2 -> {
                writeGrouping.write(fileNamePengelompokkan);
                done.show();
            }
            case 3 -> {
                writeMMM.write(fileNameSebaranData);
                writeGrouping.write(fileNamePengelompokkan);
                done.show();
            }
            default -> {
                System.err.println("Pilihan tidak dikenali! silahkan pilih lagi");
                this.show();
            }
        }
    }
}
