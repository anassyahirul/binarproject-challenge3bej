package com.app.calculation;

import java.util.List;

@FunctionalInterface
public interface Calculation<T> {
    T calc(List<Integer> data);
}
