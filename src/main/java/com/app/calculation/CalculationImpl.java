package com.app.calculation;

import com.app.calculation.Calculation;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class CalculationImpl {

    public Double getMean(List<Integer> data){
        Calculation<Double> calculation = data1 -> data1.stream()
                .mapToDouble(value -> value)
                .average()
                .orElse(0.0);

        return calculation.calc(data);
    }

    public Double getMedian(List<Integer> data) {

        Calculation<Double> calculation = data1 -> {
            Collections.sort(data1);
            if (data1.size() % 2 == 1)
                return (double) data1.get(data1.size() / 2);
            else
                return ((double) (data1.get(data1.size() / 2) + data1.get((data1.size() / 2) - 1))) / 2;
        };

        return calculation.calc(data);
    }

    public Integer getModus(List<Integer> data) {

        Calculation<Integer> calculation = data1 -> {
            int maxValue = 0, maxCount = 0, count;
            for (Integer number : data1) {
                count = 0;
                for (Integer number2 : data1) {
                    if (Objects.equals(number2, number)){
                        count++;
                    }
                }
                if (count > maxCount) {
                    maxCount = count;
                    maxValue = number;
                }
            }
            return maxValue;
        };
        return calculation.calc(data);

    }
}
