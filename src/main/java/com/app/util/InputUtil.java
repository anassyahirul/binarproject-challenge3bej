package com.app.util;

import java.util.InputMismatchException;
import java.util.Scanner;

public class InputUtil{

    Scanner scan = new Scanner(System.in);


    public byte promptInput() {

        try {
            System.out.print("---->");
            return scan.nextByte();
        } catch (InputMismatchException IME) {
            throw new InputMismatchException();
        }

    }
}
