package com.app.util;

import java.util.function.Predicate;

public class IsNumericUtil implements Predicate<String> {

    @Override
    public boolean test(String word) {
        try {
            Integer.valueOf(word);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}
