package com.app.util;

import java.io.File;

public class CreateDirectoryUtil {

    public void createDir(String resultDirectory) {
        File newDirectory = new File(resultDirectory);
        if (!newDirectory.exists()){
            newDirectory.mkdir();
        }
    }

}
