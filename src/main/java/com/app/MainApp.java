package com.app;

import com.app.view.ViewMainMenu;

public class MainApp {

    public static void main(String[] args) {
        ViewMainMenu mainMenu = new ViewMainMenu();
        mainMenu.show();
    }

}
