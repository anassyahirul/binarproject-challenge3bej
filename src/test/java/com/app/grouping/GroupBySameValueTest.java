package com.app.grouping;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(value = TestInstance.Lifecycle.PER_CLASS)
    // diubah Test LifeCycle menjadi per class, maka instance test hanya dibuat sekali
    // diubah karena terdapat ketergantungan method getGroupedDataTest() dengan method putDataTest()
class GroupBySameValueTest {

    GroupBySameValue groupBySameValue = new GroupBySameValue();

    @Test
    @Order(2)
    void getGroupedDataTest() {
        HashMap<Integer, Integer> integerHashMap = new HashMap<>();
        integerHashMap.put(1, 1);
        integerHashMap.put(2, 2);
        integerHashMap.put(3, 3);
        integerHashMap.put(4, 4);

        HashMap<Integer, Integer> result= groupBySameValue.getGroupedData();
        assertEquals(integerHashMap, result);

    }

    @Test
    @Order(1)
    void putDataTest() {
        List<Integer> example = Arrays.asList(1, 2, 2, 3, 3, 3, 4, 4, 4, 4);
        groupBySameValue.putData(example);
    }
}