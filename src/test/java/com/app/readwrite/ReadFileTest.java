package com.app.readwrite;

import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ReadFileTest {

    ReadFile readFile = new ReadFile();

    @Test
        // Method Read di test untuk membaca isi file sembarang
        // Dilakukan pembuatan Folder baru --> pembuatan File baru --> penulisan String 2 line -> pemanggilan method Read()
    void read() {
        String newDirectory = "src/test/java/com/app/readwrite/TestFolder";
        File createDirectory = new File(newDirectory);
        createDirectory.mkdir();

        try {
            File newFile = new File("src/test/java/com/app/readwrite/TestFolder/TestFile.txt");
            newFile.createNewFile();
            FileWriter fileWriter = new FileWriter(newFile);
            BufferedWriter bwr = new BufferedWriter(fileWriter);
            bwr.write("START; 4; 6; 7; 9; 1; END");
            bwr.newLine();
            bwr.write("START; 3; 4; 5; 6; 7; MID; 8; 10; 9; 4; END");
            bwr.flush();
            bwr.close();
        } catch (Exception e){
            e.printStackTrace();
        }

        List<Integer> example = readFile.read("src/test/java/com/app/readwrite/TestFolder/TestFile.txt", "; ");

        assertEquals(Arrays.asList(4, 6, 7, 9, 1, 3, 4, 5, 6, 7, 8, 10, 9, 4), example);
    }
}