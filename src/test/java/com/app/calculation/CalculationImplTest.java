package com.app.calculation;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CalculationImplTest {

    CalculationImpl calculation = new CalculationImpl();

    @Test
    void getMeanTestPositive() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        Double result = calculation.getMean(list);
        assertEquals(5.0, result);
    }


    @Test
        // Test untuk mengetahui apakah penggunaan OptionalClass .OrElse() berhasil atau tidak
    void getMeanTestWhenListIsEmpty() {
        Double result = calculation.getMean(List.of());
        assertEquals(0.0, result);
    }

    @Test
    void getMeanTestNegative() {
        Double result = calculation.getMean(List.of(1, 2, 3, 4)); // seharusnya mean = 2.5
        assertNotEquals(3.0, result);
    }

    @Test
    void getMedianTestPositive() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        Double result = calculation.getMedian(list);
        assertEquals(5.0, result);
    }

    @Test
        // Ditest apakah data pada List diurutkan dulu atau tidak, sebelum dicari nilai tengahnya
    void getMedianTestNegative() {
        List<Integer> list = Arrays.asList(5, 8, 4, 9, 1, 2, 3, 6, 7); //Nilai tengah tanpa diurutkan yaitu 1.0(Double Format)
        Double result = calculation.getMedian(list);
        assertNotEquals(1.0, result);
    }

    @Test
    void getModusTest() {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5, 7);
        Integer result = calculation.getModus(list);
        assertEquals(4, result);
    }


}