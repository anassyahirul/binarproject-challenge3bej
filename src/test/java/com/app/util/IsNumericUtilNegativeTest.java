package com.app.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IsNumericUtilNegativeTest {

    IsNumericUtil isNumericUtil = new IsNumericUtil();

    @Test
    @DisplayName("negative test case")
        // bernilai false saat input fungsi test berupa String yang tidak bisa di convert ke Integer
    void test1() {
        String nonNumericString = "Hello World!";
        Boolean result = isNumericUtil.test(nonNumericString);
        assertEquals(false, result);
    }
}