package com.app.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IsNumericUtilPositiveTest {

    IsNumericUtil isNumericUtil = new IsNumericUtil();


    @Test
    @DisplayName("positive test case")
        // bernilai true saat input fungsi test berupa String yang bisa di convert ke Integer
    void test1() {
        String numericString = "45";
        Boolean result = isNumericUtil.test(numericString);
        assertEquals(true, result);
    }

}